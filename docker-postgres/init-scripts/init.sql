CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username VARCHAR(50) UNIQUE,
    email VARCHAR(100) UNIQUE,
    password_hash VARCHAR(32),
    created_at TIMESTAMP
);

CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    role_name VARCHAR(50)
);

CREATE TABLE userroles (
    user_id INT,
    role_id INT,
    FOREIGN KEY (user_id) REFERENCES users(id),
    FOREIGN KEY (role_id) REFERENCES roles(id),
    PRIMARY KEY (user_id, role_id)
);

INSERT INTO users (username, email, password_hash, created_at) VALUES
('admin', 'admin@example.com', MD5('admin'), CURRENT_TIMESTAMP),
('user1', 'user1@example.com', MD5('password'), CURRENT_TIMESTAMP),
('user2', 'user2@example.com', MD5('password'), CURRENT_TIMESTAMP);

INSERT INTO roles (id, role_name) VALUES
(1, 'USER'),
(2, 'ADMIN');

INSERT INTO userroles (user_id, role_id) VALUES
(1, 2), -- user1 - admin
(2, 1), -- user2 - user
(3, 1); -- user3 - user