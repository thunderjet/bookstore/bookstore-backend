## Local Deployment Instructions

### Running PostgreSQL from Docker

1. Navigate to the `docker-postgres` directory in your terminal.
2. Run the following command to start PostgreSQL in detached mode:
   ```sh
   docker-compose up -d
3. PostgreSQL will be running on port 5433.
- Database name: testbd
- Username: testuser
- Password: testpassword

### Default Admin User

One admin user will be created by default with the following credentials:
- Username: admin
- Password: admin

### Non-Docker PostgreSQL Setup

1. If you are running PostgreSQL outside of Docker, edit the `resources/ConnectionManager.properties` file accordingly.
   