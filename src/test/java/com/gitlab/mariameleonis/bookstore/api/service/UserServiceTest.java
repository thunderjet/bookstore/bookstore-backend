package com.gitlab.mariameleonis.bookstore.api.service;

import com.gitlab.mariameleonis.bookstore.api.dao.UserDAO;
import com.gitlab.mariameleonis.bookstore.api.dao.UserRoleDAO;
import com.gitlab.mariameleonis.bookstore.api.dto.request.CreateUserRequest;
import com.gitlab.mariameleonis.bookstore.api.entity.UserEntity;
import com.gitlab.mariameleonis.bookstore.api.entity.UserRoleEntity;
import com.gitlab.mariameleonis.bookstore.api.service.model.CreatedUserResponse;
import com.gitlab.mariameleonis.bookstore.db.ConnectionManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;

import static com.gitlab.mariameleonis.bookstore.api.service.UserService.DEFAULT_USER_ROLE_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class UserServiceTest {
    @Mock
    private UserDAO mockUserDAO;

    @Mock
    private UserRoleDAO mockUserRoleDAO;

    @Mock
    private ConnectionManager mockConnectionManager;

    private UserService userService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        userService = new UserService(mockUserDAO, mockUserRoleDAO);
    }

    @Test
    public void testCreateUser() throws SQLException {
        // Create a mock user request
        CreateUserRequest mockUserRequest = new CreateUserRequest("testuser", "test@example.com", "password");

        // Create mock entities for user and user role
        UserEntity mockUserEntity = new UserEntity(1L, "testuser", "test@example.com", "password", new Timestamp(System.currentTimeMillis()));
        UserRoleEntity mockUserRoleEntity = new UserRoleEntity(1L, 1L);

        // Mock connection and its behavior
        Connection mockConnection = mock(Connection.class);
        when(mockConnectionManager.takeConnection()).thenReturn(mockConnection);

        // Mock DAO methods
        when(mockUserDAO.create(any(UserEntity.class), any(Connection.class))).thenReturn(mockUserEntity);

        // Call the createUser method
        CreatedUserResponse createdUserResponse = userService.createUser(mockUserRequest);

        // Verify that DAO methods were called with correct parameters
        verify(mockUserDAO, times(1)).create(any(UserEntity.class), any(Connection.class));
        verify(mockUserRoleDAO, times(1)).create(any(UserRoleEntity.class), any(Connection.class));

        // Verify the response
        assertEquals(1L, createdUserResponse.id().longValue());
        assertEquals("testuser", createdUserResponse.username());
        assertEquals("test@example.com", createdUserResponse.email());
        assertEquals(DEFAULT_USER_ROLE_NAME, createdUserResponse.roles().get(0).roleName());
    }
}
