package com.gitlab.mariameleonis.rester;

import com.sun.net.httpserver.HttpServer;
import com.gitlab.mariameleonis.bookstore.api.router.RouterFactory;
import com.gitlab.mariameleonis.rester.routing.Router;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Server {
    int port = 8080;
    Router router = RouterFactory.buildRouter();
    int threadPoolSize = 10;
    int maxThreadPoolSize = 300;


    public void start() {
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        executor.setCorePoolSize(threadPoolSize);
        executor.setMaximumPoolSize(maxThreadPoolSize);
        executor.setKeepAliveTime(60, TimeUnit.SECONDS);
        HttpServer server;
        try {
            server = HttpServer.create(new InetSocketAddress(port), 0);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        server.setExecutor(executor);
        router.build(server);
        server.start();
    }
}
