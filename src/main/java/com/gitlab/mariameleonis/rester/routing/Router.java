package com.gitlab.mariameleonis.rester.routing;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.util.HashMap;
import java.util.Map;

public class Router {
    private final Map<String, HttpHandler> routes = new HashMap<>();

    public void addRoute(String path, HttpHandler handler) {
        routes.put(path, handler);
    }

    public void build(HttpServer server) {
        routes.forEach(server::createContext);
    }

}
