package com.gitlab.mariameleonis.rester;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;

public abstract class HttpController implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {

        try {
            String method = exchange.getRequestMethod();
            switch (method) {
                case "GET" -> doGet(exchange);
                case "POST" -> doPost(exchange);
                case "PUT" -> doPut(exchange);
                case "PATCH" -> doPatch(exchange);
                case "DELETE" -> doDelete(exchange);
                default -> returnDefaultResponse(exchange);
            }
        } catch (Exception ex) {
            returnResponse(500, ex.getClass().getSimpleName(), exchange);
        }

    }

    protected void doDelete(HttpExchange exchange) {
        returnDefaultResponse(exchange);
    }

    private void doPatch(HttpExchange exchange) {
        returnDefaultResponse(exchange);
    }

    protected void doPut(HttpExchange exchange) {
        returnDefaultResponse(exchange);
    }

    protected void doPost(HttpExchange exchange) throws Exception {
        returnDefaultResponse(exchange);
    }

    protected void doGet(HttpExchange exchange) {
        returnDefaultResponse(exchange);
    }

    private void returnDefaultResponse(HttpExchange exchange) {
        returnResponse(405, "Method Not Allowed!", exchange);
    }

    protected final void returnResponse(int statusCode, String body, HttpExchange exchange) {
        try {
            exchange.getResponseHeaders().add("Access-Control-Allow-Origin", "*");
            exchange.sendResponseHeaders(statusCode, body.length());
            exchange.getResponseBody().write(body.getBytes());
        } catch (IOException e) {
            // TODO consider more robust logging
            e.printStackTrace();
        }
    }

    protected final String getRequestBodyAsString(HttpExchange exchange) throws IOException {
        return new String(exchange.getRequestBody().readAllBytes());
    }
}
