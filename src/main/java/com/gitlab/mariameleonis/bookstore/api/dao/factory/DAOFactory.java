package com.gitlab.mariameleonis.bookstore.api.dao.factory;

import com.gitlab.mariameleonis.bookstore.api.dao.impl.UserRoleDAOImpl;
import com.gitlab.mariameleonis.bookstore.api.dao.BaseDAO;
import com.gitlab.mariameleonis.bookstore.api.dao.impl.UserDAOImpl;

public enum DAOFactory {
    USER_DAO(new UserDAOImpl()),
    USER_ROLE_DAO(new UserRoleDAOImpl());

    private final BaseDAO baseDAO;

    DAOFactory(BaseDAO DAOImpl) {
        baseDAO = DAOImpl;
    }

    public BaseDAO getDAO() {
        return baseDAO;
    }
}
