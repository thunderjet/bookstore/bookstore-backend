package com.gitlab.mariameleonis.bookstore.api.dao;

import com.gitlab.mariameleonis.bookstore.api.entity.UserEntity;

import java.sql.Connection;

public interface UserDAO extends BaseDAO {
    UserEntity create(UserEntity user, Connection conn);
}
