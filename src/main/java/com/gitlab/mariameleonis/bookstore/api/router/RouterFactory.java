package com.gitlab.mariameleonis.bookstore.api.router;

import com.gitlab.mariameleonis.bookstore.api.controller.HelloController;
import com.gitlab.mariameleonis.bookstore.api.controller.factory.ControllerFactory;
import com.gitlab.mariameleonis.rester.routing.Router;
import com.gitlab.mariameleonis.bookstore.api.controller.UserController;

public class RouterFactory {

    public static Router buildRouter() {
        Router router = new Router();

        router.addRoute("/api/hello-world",  (HelloController) ControllerFactory.HELLO_CONTROLLER.getController());
        router.addRoute("/api/users", (UserController) ControllerFactory.USER_CONTROLLER.getController());

        return router;
    }
}
