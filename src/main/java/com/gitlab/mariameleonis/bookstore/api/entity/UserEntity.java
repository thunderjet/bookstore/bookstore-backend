package com.gitlab.mariameleonis.bookstore.api.entity;

import java.sql.Timestamp;
import java.time.Instant;

public class UserEntity {
    private Long id;
    private String username;
    private String email;
    private String password;
    private Timestamp createdAt;

    public UserEntity() {
        // Default constructor
    }

    public UserEntity(Long id, String username, String email, String password, Timestamp createdAt) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.createdAt = createdAt;
    }

    public UserEntity(String username, String email, String password, Timestamp createdAt) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.createdAt = createdAt;
    }

    // Getters and setters for all fields


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    // toString, equals, hashCode methods (optional, but recommended for POJOs)
}
