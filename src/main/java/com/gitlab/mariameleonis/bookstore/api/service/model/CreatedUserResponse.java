package com.gitlab.mariameleonis.bookstore.api.service.model;

import java.sql.Timestamp;
import java.util.List;

public record CreatedUserResponse(Long id, String username, String email, Timestamp createdAt, List<Role> roles) {
}
