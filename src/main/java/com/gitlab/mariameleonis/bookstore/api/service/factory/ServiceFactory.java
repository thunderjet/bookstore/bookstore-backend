package com.gitlab.mariameleonis.bookstore.api.service.factory;

import com.gitlab.mariameleonis.bookstore.api.dao.UserDAO;
import com.gitlab.mariameleonis.bookstore.api.dao.UserRoleDAO;
import com.gitlab.mariameleonis.bookstore.api.dao.factory.DAOFactory;
import com.gitlab.mariameleonis.bookstore.api.service.UserService;
import com.gitlab.mariameleonis.bookstore.api.service.BaseService;

public enum ServiceFactory {
    USER_SERVICE(new UserService((UserDAO) DAOFactory.USER_DAO.getDAO(), (UserRoleDAO) DAOFactory.USER_ROLE_DAO.getDAO()));

    private final BaseService baseService;


    ServiceFactory(BaseService baseService) {
        this.baseService = baseService;
    }

    public BaseService getService() {
        return baseService;
    }
}
