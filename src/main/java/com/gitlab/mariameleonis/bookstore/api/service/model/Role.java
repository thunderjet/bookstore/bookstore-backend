package com.gitlab.mariameleonis.bookstore.api.service.model;

public record Role(String roleName) {
}
