package com.gitlab.mariameleonis.bookstore.api.service;

import com.gitlab.mariameleonis.bookstore.api.dto.request.CreateUserRequest;
import com.gitlab.mariameleonis.bookstore.api.dao.UserDAO;
import com.gitlab.mariameleonis.bookstore.api.dao.UserRoleDAO;
import com.gitlab.mariameleonis.bookstore.api.entity.UserEntity;
import com.gitlab.mariameleonis.bookstore.api.entity.UserRoleEntity;
import com.gitlab.mariameleonis.bookstore.api.service.model.CreatedUserResponse;
import com.gitlab.mariameleonis.bookstore.api.service.model.Role;
import com.gitlab.mariameleonis.bookstore.db.ConnectionManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

public class UserService implements BaseService {

    public static final Long DEFAULT_USER_ROLE_ID = 1L;
    public static final String DEFAULT_USER_ROLE_NAME = "USER";
    private final UserDAO userDAO;
    private final UserRoleDAO userRoleDAO;
    private final ConnectionManager connectionManager = ConnectionManager.getInstance();

    public UserService(UserDAO userDAO, UserRoleDAO userRoleDAO) {
        this.userDAO = userDAO;
        this.userRoleDAO = userRoleDAO;
    }


    public CreatedUserResponse createUser(CreateUserRequest user) throws SQLException {
        Connection connection = connectionManager.takeConnection();
        connection.setAutoCommit(false);
        UserEntity userEntity = new UserEntity(user.username(), user.email(), user.password(), new Timestamp(System.currentTimeMillis()));
        UserEntity createdUser = userDAO.create(userEntity, connection);

        UserRoleEntity userRoleEntity = new UserRoleEntity(createdUser.getId(), DEFAULT_USER_ROLE_ID);
        userRoleDAO.create(userRoleEntity, connection);

        connection.commit();
        connectionManager.returnConnection(connection);
        return new CreatedUserResponse(createdUser.getId(), createdUser.getUsername(), createdUser.getEmail(), createdUser.getCreatedAt(), List.of(new Role(DEFAULT_USER_ROLE_NAME)));
    }
}
