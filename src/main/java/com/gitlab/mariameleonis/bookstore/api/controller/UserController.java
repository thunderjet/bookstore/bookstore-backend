package com.gitlab.mariameleonis.bookstore.api.controller;

import com.gitlab.mariameleonis.bookstore.api.dto.request.CreateUserRequest;
import com.gitlab.mariameleonis.bookstore.api.service.UserService;
import com.gitlab.mariameleonis.bookstore.api.service.model.CreatedUserResponse;
import com.gitlab.mariameleonis.rester.HttpController;
import com.gitlab.mariameleonis.rester.utilities.JsonConverter;
import com.sun.net.httpserver.HttpExchange;

public class UserController extends HttpController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    protected void doPost(HttpExchange exchange) throws Exception {
        String requestBody = getRequestBodyAsString(exchange);
        CreateUserRequest userRequest;
        try {
            userRequest = JsonConverter.fromJson(requestBody, CreateUserRequest.class);
        } catch (JsonConverter.JsonConverterException e) {
            returnResponse(400, "Invalid request", exchange);
            return;
        }

        CreatedUserResponse createdUser;
        try {
            createdUser = userService.createUser(userRequest);
        } catch (Exception e) {
            returnResponse(500, "Failed to create user", exchange);
            return;
        }

        returnResponse(200, JsonConverter.toJson(createdUser), exchange);
    }
}
