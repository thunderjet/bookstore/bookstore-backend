package com.gitlab.mariameleonis.bookstore.api.dao;

import com.gitlab.mariameleonis.bookstore.api.entity.UserRoleEntity;

import java.sql.Connection;

public interface UserRoleDAO extends BaseDAO {
    void create(UserRoleEntity userRole, Connection connection);
}
