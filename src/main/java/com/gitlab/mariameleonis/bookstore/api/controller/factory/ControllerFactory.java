package com.gitlab.mariameleonis.bookstore.api.controller.factory;

import com.gitlab.mariameleonis.bookstore.api.controller.HelloController;
import com.gitlab.mariameleonis.bookstore.api.service.UserService;
import com.gitlab.mariameleonis.rester.HttpController;
import com.gitlab.mariameleonis.bookstore.api.controller.UserController;
import com.gitlab.mariameleonis.bookstore.api.service.factory.ServiceFactory;

public enum ControllerFactory {
    HELLO_CONTROLLER(new HelloController()),
    USER_CONTROLLER(new UserController((UserService) ServiceFactory.USER_SERVICE.getService()));

    private final HttpController httpController;


    ControllerFactory(HttpController httpController) {
        this.httpController = httpController;
    }

    public HttpController getController() {
        return httpController;
    }
}
