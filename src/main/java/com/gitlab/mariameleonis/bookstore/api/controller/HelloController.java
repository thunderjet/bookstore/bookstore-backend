package com.gitlab.mariameleonis.bookstore.api.controller;

import com.gitlab.mariameleonis.rester.HttpController;
import com.sun.net.httpserver.HttpExchange;

public class HelloController extends HttpController {

    @Override
    protected void doGet(HttpExchange exchange) {
        returnResponse(200, "Hello World!!!", exchange);
    }
}
