package com.gitlab.mariameleonis.bookstore.api.dao.impl;

import com.gitlab.mariameleonis.bookstore.api.dao.AbstractBaseDAO;
import com.gitlab.mariameleonis.bookstore.api.dao.UserRoleDAO;
import com.gitlab.mariameleonis.bookstore.api.entity.UserRoleEntity;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRoleDAOImpl extends AbstractBaseDAO<UserRoleEntity> implements UserRoleDAO {
    private static final String CREATE_USER_ROLE = "INSERT INTO USERROLES (user_id, role_id) values (?,?)";
    @Override
    public void create(UserRoleEntity userRole, Connection connection) {
        executeUpdate(CREATE_USER_ROLE, connection, userRole.getUserId(), userRole.getRoleId());
    }

    @Override
    protected UserRoleEntity parseResultSet(ResultSet resultSet) throws SQLException {
        return null;
    }
}
