package com.gitlab.mariameleonis.bookstore.api.dto.request;

public record CreateUserRequest(String username, String email, String password) {
}
