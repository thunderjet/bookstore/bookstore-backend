package com.gitlab.mariameleonis.bookstore.api.dto.response;

import java.time.Instant;

public record UserDTO(String username, String email, String password, Instant createdAt) {
}
