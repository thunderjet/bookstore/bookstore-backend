package com.gitlab.mariameleonis.bookstore.api.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractBaseDAO<E> {
    private final Logger LOGGER = LogManager.getLogger(this.getClass().getName());

    protected abstract E parseResultSet(ResultSet resultSet) throws SQLException;

    E executeStatementAndParseResultSet(PreparedStatement statement) throws SQLException {
        E entity;
        try (ResultSet resultSet = statement.executeQuery()) {
            if (resultSet.next()) {
                entity = parseResultSet(resultSet);
            } else {
                entity = null;
            }
        }
        return entity;
    }

    List<E> executeStatementAndParseResultSetToList(PreparedStatement statement) throws SQLException {
        ArrayList<E> entities = new ArrayList<>();
        try (ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                entities.add(parseResultSet(resultSet));
            }
        }
        entities.trimToSize();
        return entities;
    }

    E getByParameters(String sqlQuery, Connection connection, Object... parameters) {
        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            populatePreparedStatement(statement, parameters);
            return executeStatementAndParseResultSet(statement);
        } catch (SQLException e) {
            LOGGER.error("Failed to select row from database", e);
            throw new UnsupportedOperationException(e);
        }
    }

    List<E> getAll(String sqlQuery, Connection connection, Object... parameters) {
        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            populatePreparedStatement(statement, parameters);
            return executeStatementAndParseResultSetToList(statement);
        } catch (SQLException e) {
            LOGGER.error("Failed to select rows from database", e);
            throw new UnsupportedOperationException(e);
        }
    }

    protected Long executeUpdateReturnKey(String sqlQuery, Connection connection, Object... parameters) {
        try (PreparedStatement statement = connection.prepareStatement(sqlQuery, PreparedStatement.RETURN_GENERATED_KEYS)) {
            populatePreparedStatement(statement, parameters);
            statement.executeUpdate();
            try (ResultSet keys = statement.getGeneratedKeys()) {
                if (keys.next()) {
                    return keys.getLong(1);
                } else {
                    LOGGER.warn("Cannot return generated keys for specified sqlQuery parameter");
                    throw new UnsupportedOperationException();
                }
            }
        } catch (SQLException e) {
            LOGGER.error("Failed to update row to database", e);
            throw new UnsupportedOperationException(e);
        }
    }

    protected void executeUpdate(String sqlQuery, Connection connection, Object... parameters) {
        try (PreparedStatement statement = connection.prepareStatement(sqlQuery)) {
            populatePreparedStatement(statement, parameters);
            int result = statement.executeUpdate();
            LOGGER.info("Updated " + result + " rows");
        } catch (SQLException e) {
            LOGGER.error("Failed to update row to database", e);
            throw new UnsupportedOperationException(e);
        }
    }


    private void populatePreparedStatement(PreparedStatement statement, Object... parameters) throws SQLException {
        if (parameters != null) {
            for (int i = 0; i < parameters.length; i++) {
                try {
                    statement.setObject(i + 1, parameters[i]);
                } catch (SQLException e) {
                    LOGGER.error("Failed to populate prepare statement with specified values");
                    throw new UnsupportedOperationException(e);
                }
            }
        }
    }
}
