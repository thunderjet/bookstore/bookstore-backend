package com.gitlab.mariameleonis.bookstore.api.dao.impl;

import com.gitlab.mariameleonis.bookstore.api.dao.UserDAO;
import com.gitlab.mariameleonis.bookstore.api.entity.UserEntity;
import com.gitlab.mariameleonis.bookstore.api.dao.AbstractBaseDAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAOImpl extends AbstractBaseDAO<UserEntity> implements UserDAO {
    private static final String CREATE_USER = "INSERT INTO USERS (username, email, password_hash, created_at) VALUES (?,?,?,?)";
    @Override
    public UserEntity create(UserEntity user, Connection conn) {
        Long id = executeUpdateReturnKey(CREATE_USER, conn, user.getUsername(), user.getEmail(), user.getPassword(), user.getCreatedAt());
        user.setId(id);
        return user;
    }

    @Override
    protected UserEntity parseResultSet(ResultSet resultSet) throws SQLException {
        return null;
    }
}
