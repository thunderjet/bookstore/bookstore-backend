package com.gitlab.mariameleonis.bookstore;

import com.gitlab.mariameleonis.rester.Server;

public class Main {
    public static void main(String[] args) {
        Server server = new Server();
        server.start();
        System.out.println("Service started");
    }

}
